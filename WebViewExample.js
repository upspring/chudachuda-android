import React, { Component } from 'react'
import { View, StyleSheet, Dimensions, Text, AsyncStorage, BackHandler, ActivityIndicator, Image, Linking }from 'react-native';
import { Icon, Button, Container, Header, Content, Left } from 'native-base'
//import { NavigationBar } from 'navigationbar-react-native';
import ContentLoader from 'react-native-content-loader'
import Svg, {Circle, Rect } from 'react-native-svg'
const SCREEN_HEIGHT = Dimensions.get("window").height;
import {WebView} from 'react-native-webview'

class WebViewExample extends Component {
    render(){
        return(
            <View>
                
                <Button title="Click me" onPress={ ()=>{ Linking.openURL('https://google.com')}} />
                <Button
                onPress={onPressLearnMore}
                title="Learn More"
                color="#841584"
                accessibilityLabel="Learn more about this purple button"
                />
            </View>
        )
    }
}
export default WebViewExample