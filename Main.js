import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    ImageBackground,
    AsyncStorage,
    ActivityIndicator
} from "react-native";
import NetInfo from "@react-native-community/netinfo";
import {createStackNavigator} from 'react-navigation'
import SplashScreen from 'react-native-smart-splash-screen'
import MySide from './MySide'
import FCM, { FCMEvent} from "react-native-fcm";
import TamilTheHindu from "./TamilTheHindu";
const SCREEN_HEIGHT = Dimensions.get("window").height
const SCREEN_WIDTH = Dimensions.get("window").width;
import React, {Component} from 'react';
import WebViewExample from './WebViewExample'
 


class DeckSwiper extends Component {
  
    static navigationOptions = {
       header: null,
    };
   constructor(props) {
        super(props)
        this.state = {
           isConnected:true,
           currentIndexValue:'',
           currentComponent:'',
           isLoading: false


         }
   }
  
   componentWillMount()
  {

      AsyncStorage.getItem('cIndex').then((value)=>{
        this.setState({currentIndexValue:value, isLoading: true});
        
      });
      AsyncStorage.getItem('component').then((value)=>{
        this.setState({currentComponent:value, isLoading: true});
      });
        setTimeout(() =>{
          this.setState({isLoading: false});
          console.log("header False"+this.state.isLoading);
      }, 1000);

      //NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
     
  }
  componentDidMount ()
  {
      
      //NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
        SplashScreen.close({
           animationType: SplashScreen.animationType.scale,
           duration: 750,
           delay: 200,
        })
        
        
    
        
        FCM.getInitialNotification().then(notif => {
          console.log('inside of notification listener');
          let value=JSON.stringify(notif);
          let jsonObj=JSON.parse(value);
          var title = jsonObj["title"];
          var domain = jsonObj["domain"];
        
          if(domain == "tamil.samayam.com" || domain == "tamil.thehindu.com" || domain == "dinakaran.com" || domain == "bbc.com" || domain == "oneindia.com" || domain == "puthiyathalaimurai.com")
          {
            this.props.navigation.navigate('Notification',{title: title,domain:domain});
          }
      
        });
        FCM.on(FCMEvent.Notification, notif => {
          console.log('inside of notification event');
          let value=JSON.stringify(notif);
          let jsonObj=JSON.parse(value);
          var title = jsonObj["title"];
          var domain = jsonObj["domain"];
         
          if(domain == "tamil.samayam.com" || domain == "tamil.thehindu.com" || domain == "dinakaran.com" || domain == "bbc.com" || domain == "oneindia.com" || domain == "puthiyathalaimurai.com")
          {
            this.props.navigation.navigate('Notification',{title: title,domain:domain});
          }
        

        });
  }
  handleConnectivityChange = isConnected => {
     if (isConnected) {
        this.setState({ isConnected });
      } else {
        this.setState({ isConnected });
       }
  };
    render() {
      console.disableYellowBox = true;
        
          if(this.state.currentIndexValue!==-1&&this.state.currentComponent!==""&&this.state.currentComponent!=null&&this.state.currentIndexValue!=null)
            {
              console.log('inside of state maintananace');
              var index=this.state.currentIndexValue.replace(/['"]+/g,'');
              return(
                  this.state.isLoading
                  ?
                      <ImageBackground source={require('./assets/load-background.png')} style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                          <View style={{width:120, height: 90}}>
                              <ActivityIndicator size="large" color="#fff" style={{marginBottom: 10, marginTop: 10, elevation: 10}} animating />
                              <Text style={{marginTop: 0, fontSize: 20, color: 'white', marginLeft: 20, marginBottom: 10, marginTop: 15, elevation: 10 }}>Loading...</Text>
                          </View>
                      </ImageBackground>
                  :
                  this.props.navigation.navigate(this.state.currentComponent.replace(/['"]+/g,''),{newValue:index})
               )
             
            }
            else{
              return(
                <MySide/>
               )
            }
      
    }
}


export default Project = createStackNavigator(
{
   Main: { screen: DeckSwiper },
   SideBar: {screen: MySide,
      navigationOptions: {
         header: null
       }
    }
    
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'green'
    },
    img:{
        width:450,
        height:450,
        flex: 1
    }
});