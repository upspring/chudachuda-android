import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    ImageBackground,
    AsyncStorage,
    ActivityIndicator,
    Button
} from "react-native";
import NetInfo from "@react-native-community/netinfo";
import {createStackNavigator} from 'react-navigation'
import SplashScreen from 'react-native-smart-splash-screen'
import MySide from './MySide'
import FCM, { FCMEvent} from "react-native-fcm";
import TamilTheHindu from "./TamilTheHindu";
const SCREEN_HEIGHT = Dimensions.get("window").height
const SCREEN_WIDTH = Dimensions.get("window").width;
import { NetworkConsumer } from 'react-native-offline';
import React, {Component} from 'react';
import Main from  './Main';



 class Network extends Component {
    render() {
      console.disableYellowBox = true;
            return(
              <NetworkConsumer>
              {({ isConnected }) => (
              isConnected ? (
                <Main />
              ) : (
                <ImageBackground source={require('./assets/load-background.png')} style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                <View style={{height: SCREEN_HEIGHT, width: SCREEN_WIDTH}}>
                  <View style={{ marginLeft: 10, marginRight: 10, marginTop: 50}}>
                      <Text style={{color: '#ffff00',textAlign: 'center',fontWeight: 'bold', fontSize: 20, lineHeight: 35, marginTop: 10, marginBottom: 10, elevation: 10}}>இணையம் இல்லை உங்கள் டேட்டா அல்லது வைபை ஆன் செய்யவும்</Text>
                  </View>
                </View>
              </ImageBackground>
              )
              )}
            </NetworkConsumer>
            )
    }
}
export default Network

