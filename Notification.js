import React from 'react'
import { View, Text,StyleSheet,Image,Animated,Dimensions,PanResponder, Linking, ImageBackground, ActivityIndicator,TouchableHighlight, WebView} from "react-native";
class Notification extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: "சமயம்",
    drawerLabel: <Text style={{fontSize: 14,color: '#fff', paddingLeft: 15, fontWeight: 'bold', paddingTop: 20, paddingBottom: 20}}></Text>,
  })
    constructor(props) {
        super(props)
    }
  
  renderRedirect = () => {
    console.log('inside of notification component');
   
    const{navigation}= this.props.navigation;
   var title= this.props.navigation.getParam('title');
    var domain= this.props.navigation.getParam('domain');
   
        if(domain == "tamil.samayam.com")
         {
           this.props.navigation.navigate('Samayam',{title: title});
         }
         if(domain == "tamil.thehindu.com")
         {
           
           this.props.navigation.navigate('TamilTheHindu',{title: title});
         }
         if(domain == "dinakaran.com")
         {
           this.props.navigation.navigate('Dinakaran',{title: title});
         }
        if(domain == "bbc.com")
        {
          this.props.navigation.navigate('BBCTamil',{title: title});
        }
        if(domain == "oneindia.com")
        {
          this.props.navigation.navigate('OneIndia',{title: title});
        }
        if(domain == "puthiyathalaimurai.com")
        {
          this.props.navigation.navigate('Puthiyathalaimurai',{title: title});
        }
    
  }
  render () {
    return (
      <View>
        {this.renderRedirect()}
      
        </View>
    )
  }
}
export default Notification;
