import React, { Component } from 'react'
import { View, StyleSheet, Dimensions, Text, AsyncStorage, BackHandler, ActivityIndicator, Image }from 'react-native';
import { Icon, Button, Container, Header, Content, Left } from 'native-base'
//import { NavigationBar } from 'navigationbar-react-native';
import ContentLoader from 'react-native-content-loader'
import Svg, {Circle, Rect } from 'react-native-svg'
const SCREEN_HEIGHT = Dimensions.get("window").height;
import {WebView} from 'react-native-webview'

class WebViewExample extends Component {
   static navigationOptions = ({ navigation }) => ({
      title: "",
      headerLeft: <Icon name="ios-menu" style={{ paddingLeft: 10 }} onPress={() => navigation.navigate('DrawerOpen')} />,
      drawerLabel: <Text style={{fontSize: 14,color: '#fff', paddingLeft: 15, fontWeight: 'bold', paddingTop: 20, paddingBottom: 20}}></Text>,
   })
   constructor(props) {
    super(props)
    this.state ={
        date: '',
        time: '',
        weeklyDay: '',
        isLoading: true
    }
   }
   handleAndroidBackButton = () => {
      BackHandler.addEventListener('hardwareBackPress', () => {
        this.backPressed();
        return true;
      });
    };
     navigateBack() {
     
    }
   backPressed = () => {
     
      var CardPosition = this.props.navigation.getParam('cardPosition');
      var url= this.props.navigation.getParam('url');
      console.log("URL",url);
      var newsTitle = this.props.navigation.getParam('title');
      var backComp= this.props.navigation.getParam('component');
      var index= this.props.navigation.getParam('index');
      var Component = this.props.navigation.getParam('cname');
      this.props.navigation.navigate(Component,{newsTitle: newsTitle, CardPosition: CardPosition});
   }
   GoBack = data => {
    this.setState(data);
  };
   componentDidMount() {
      setTimeout(() => this.setState({isLoading: !this.state.isLoading}), 2000);
      this.handleAndroidBackButton(this.navigateBack());

      var that = this;
      var day = new Date().getDay();
      var date = new Date().getDate(); //Current Date
      var month = new Date().getMonth() + 1; //Current Month
      var year = new Date().getFullYear(); //Current Year
      var hours = new Date().getHours() -12; //Current Hours
      var min = new Date().getMinutes(); //Current Minutes
      var sec = new Date().getSeconds(); //Current Seconds
      that.setState({
      //Setting the value of the date time
      date:
          date + '/' + month + '/' + year,
      });
      this.Clock = setInterval( () => this.GetTime(), 1000 );

      
   }
   componentWillMount(){
      clearInterval(this.Clock);

      var d = new Date();
      var weekday = new Array(7);
      weekday[0] = "Sun";
      weekday[1] = "Mon";
      weekday[2] = "Tue";
      weekday[3] = "Wed";
      weekday[4] = "Thu";
      weekday[5] = "Fri";
      weekday[6] = "Sat";

      var days = weekday[d.getDay()];
      this.setState({weeklyDay: days});
   }
   componentWillUnmount(){
      BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
   }
   GetTime() {
 
      // Creating variables to hold time.
      var date, TimeType, hour, minutes, seconds, fullTime;
   
      // Creating Date() function object.
      date = new Date();
   
      // Getting current hour from Date object.
      hour = date.getHours(); 
   
      // Checking if the Hour is less than equals to 11 then Set the Time format as AM.
      if(hour <= 11)
      {
   
        TimeType = 'AM';
   
      }
      else{
   
        // If the Hour is Not less than equals to 11 then Set the Time format as PM.
        TimeType = 'PM';
   
      }
   
   
      // IF current hour is grater than 12 then minus 12 from current hour to make it in 12 Hours Format.
      if( hour > 12 )
      {
        hour = hour - 12;
      }
   
      // If hour value is 0 then by default set its value to 12, because 24 means 0 in 24 hours time format. 
      if( hour == 0 )
      {
          hour = 12;
      } 
   
   
      // Getting the current minutes from date object.
      minutes = date.getMinutes();
   
      // Checking if the minutes value is less then 10 then add 0 before minutes.
      if(minutes < 10)
      {
        minutes = '0' + minutes.toString();
      }
   
   
      //Getting current seconds from date object.
      seconds = date.getSeconds();
   
      // If seconds value is less than 10 then add 0 before seconds.
      if(seconds < 10)
      {
        seconds = '0' + seconds.toString();
      }
   
   
      // Adding all the variables in fullTime variable.
      fullTime = hour.toString() + ':' + minutes.toString() + ':' + seconds.toString() + ' ' + TimeType.toString();
   
   
      // Setting up fullTime variable in State.
      this.setState({
   
        time: fullTime
   
      });
    }
    showSpinner(){
     
      this.setState({ isLoading: true });
    }
    hideSpinner() {
      
      this.setState({ isLoading: false });
    }
  
   render()
   {
    
    const{goBack}=this.props.navigation;
    var CardPosition = this.props.navigation.getParam('cardPosition');
    var url= this.props.navigation.getParam('url');
    var newsTitle = this.props.navigation.getParam('title');
    var backComp= this.props.navigation.getParam('component');
    var index= this.props.navigation.getParam('index');
    var Component = this.props.navigation.getParam('cname')
    
    return (
      <View style = {styles.container}>
         <View style={styles.navbar}>
            <Icon name='ios-arrow-back' size={25} style={{color: 'white'}} onTouchStart={() => this.props.navigation.navigate(Component,{onGoBack: () => this.GoBack(),newsTitle: newsTitle, CardPosition: CardPosition})}/>
            <Text style={{ color: 'white',marginLeft: 130,fontSize: 22}}>சுடசுட</Text>
            <Text style={{color: 'white', marginLeft: 30,marginTop:5,fontSize: 13}} >{this.state.weeklyDay}</Text>
            <Text style={{color: 'white', marginLeft: 5,marginTop:5,fontSize: 13}}>{this.state.date}</Text>
            <Text style={{color: 'white', marginLeft: 5,marginTop:5,fontSize: 13}}>{this.state.time}</Text>
         </View>
         {this.state.isLoading == true?
            <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#9e9e9e" height={850} width={500} duration={2000}>
               
               <Rect x="0" y="0" width="450" height="60" stroke="#ddd" strokeWidth="2" fill="#ddd" />
               <Rect x="20" y="100" rx="3" ry="3" width="360" height="10" />
               <Rect x="20" y="130" rx="3" ry="3" width="360" height="10" />
               <Rect x="20" y="160" rx="3" ry="3" width="360" height="10" />
               <Rect x="20" y="200" rx="3" ry="3" width="360" height="200" />
               <Rect x="20" y="430" rx="3" ry="3" width="360" height="7" />
               <Rect x="20" y="460" rx="3" ry="3" width="360" height="7" />
               <Rect x="20" y="490" rx="3" ry="3" width="360" height="7" />
               <Rect x="20" y="520" rx="3" ry="3" width="360" height="7" />
               <Rect x="20" y="550" rx="3" ry="3" width="360" height="7" />
               <Rect x="20" y="580" rx="3" ry="3" width="201" height="7" />
            
            </ContentLoader>:null}
            <WebView 
                  source = {{ uri:url}}
                  onLoadStart={() => this.showSpinner()}
                  onLoad={() => this.hideSpinner()}
            />
      </View>
   )
}
}
export default WebViewExample;

const styles = StyleSheet.create({
   container: {
      height: SCREEN_HEIGHT,
   },
   navbar: {
       height: 55,
       backgroundColor: 'black',
       elevation: 3,
       paddingHorizontal: 15,
       flexDirection: 'row',  
       alignItems: 'center'

   }
})



