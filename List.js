import React, {Component} from 'react';
import { Image, Button, Text, View, ImageBackground } from 'react-native';
import { NetworkConsumer } from 'react-native-offline';
import SplashScreen from 'react-native-smart-splash-screen'
import MySide from './MySide'
import FCM, { FCMEvent} from "react-native-fcm";
import TamilTheHindu from "./TamilTheHindu";

 
class List extends Component{
    render(){
        return(
            <View>
                <NetworkConsumer>
                     {({ isConnected }) => (
                     isConnected ? (
                        <Button title="Download image" />
                     ) : (
                        <ImageBackground source={require('./assets/load-background.png')} style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                            <View style={{height: 200, width: 400}}>
                                <View style={{ marginLeft: 10, marginRight: 10, marginTop: 50}}>
                                    <Text style={{color: '#ffff00',textAlign: 'center',fontWeight: 'bold', fontSize: 20, lineHeight: 35, marginTop: 10, marginBottom: 10, elevation: 10}}>இணையம் இல்லை உங்கள் டேட்டா அல்லது வைபை ஆன் செய்யவும்</Text>
                                </View>
                            </View>
                        </ImageBackground>
                     )
                     )}
               </NetworkConsumer>
            </View>
        )
    }
}
export default List