import React, { Component } from "react";
import { View, Text,StyleSheet,Image,Animated,Dimensions,PanResponder, Linking, ImageBackground, ActivityIndicator,TouchableHighlight, TouchableOpacity, AsyncStorage, BackHandler, Alert} from "react-native";
import { Icon, Button, Container, Header, Content, Left } from 'native-base'
import { DrawerNavigator, StackNavigator, DrawerItems, SafeAreaView,DrawerActions } from 'react-navigation'
import ContentLoader from 'react-native-content-loader'
import Svg, {Circle, Rect } from 'react-native-svg'
const SCREEN_HEIGHT = Dimensions.get("window").height
const SCREEN_WIDTH = Dimensions.get("window").width
const Height_width = Dimensions.get("window");
import renderIf from './renderIf'
import Share, {ShareSheet} from 'react-native-share';
import Dinakaran from "./Dinakaran";
var DeviceInfo = require('react-native-device-info').default;
var device_id=DeviceInfo.getUniqueID();
var currentBookmarkId = [];
var isCurrentBookmarked = false;
var isCurrentBookmarkId = -1;
var no_of_user_bookmark = [];
var isCurrentBookmarkedValues = -1;
var BookmarkArray = [];
var deviceBookmarkId = -1;

var  currentLikeId= [];
var isCurrentLiked=false;
var isCurrentLikedId=-1;
var no_of_user_like=[];
var isCurrentLikedValues =-1;
var deviceId=[];
var deviceLinkId=-1;
var LikeArray=[];
class Bookmark extends Component {
     _isMouted = false;
    static navigationOptions = ({ navigation }) => ({
        title: "தமிழ் தி இந்து",
        headerLeft: <Icon name="ios-menu" style={{ paddingLeft: 10 }} onPress={() => navigation.navigate('DrawerOpen')} />,
        drawerLabel: <Text style={{fontSize: 14,color: '#fff', paddingLeft: 15, fontWeight: 'bold', paddingTop: 20, paddingBottom: 20}}>சேமிக்கப்பட்ட செய்திகள்</Text>,
        drawerIcon: ({ tintColor }) => (
          <Image
            source={require('./assets/star-outline-yellow.png')}
            style={{height: 25, width: 25, borderRadius: 50}}
          />
        ),
      })
       toggleStatus = () =>  {
    }
     constructor(props) {
        super(props)
        this.position = new Animated.ValueXY()
        this.swipedCardPosition = new Animated.ValueXY({ x: 0, y: -SCREEN_HEIGHT })
        this.state = {
            currentIndex: 0,
            index:0,
            isLoading: true,
            Loading: false,
            refresh:false,
            refreshLoader:true,
            data:[],
            deviceLike:[],
            no_of_likes:0,
            deviceBookmark: [],
            id:[],
            bookmarkId:[],
            header: false,
            webLoad: false,
            stateLoad: false,
            stateLoader: false,
            ComponentName: 'Bookmark',


        };

    }
    shareMessage(message,url)
    {
        var headline=message;
        let shareOptions = {
             title: "சுடசுட",
             message: headline,
             url:url+"\n"+'\n'+'சுடசுட செயலியை பெற'+'\n'+"http://bit.ly/2ttnEKs",
             subject: "சுடசுட" //  for email
        };
        Share.open(shareOptions);
        this.setState({header: false});
    }
    getJsonData() {
      this.setState({id:[]});
      currentLikeId=[];
      LikeArray=[];
      this.setState({deviceLike:[]});
      var title= this.props.navigation.getParam('title');

        if(typeof title==='undefined')
        {

            fetch('http://www.chudachuda.com/GetBookmarks?deviceId='+device_id)
            .then(res => res.json())
            .then((responseJson) => {
                console.log("res",responseJson.feed)
                //setTimeout(() => this.setState({data:responseJson.feed,isLoading:!this.state.isLoading}),2000);
                this.setState({data:responseJson.feed, isLoading: false})
             })
            .catch(error => console.log("error", error));


        }
       else{
           fetch('http://www.chudachuda.com/GetBookmarks?deviceId='+device_id)
           .then(res => res.json())
           .then((responseJson) => {
                var data=[];
                var JSONObj = {};
                for (var i = 0; i < responseJson.feed.length; i++) {
                    var object = responseJson.feed[i];
                    for (var property in object) {
                        if(property=="title" && object[property]==title)
                        {
                           for (var property in object) {
                           JSONObj[property]=object[property];
                            }
                            data.push(JSONObj);
                        }
                    }
                }
                for(var i = 0; i < responseJson.feed.length; i++)
                  {
                    var object=responseJson.feed[i]
                    for (var property in object) {
                        if(property=="title" && object[property]!=title)
                        {
                           var JSONObj1= {};
                             for (var property in object) {
                                JSONObj1[property]=object[property]
                             }
                             data.push(JSONObj1);
                        }
                    }
                }
                this.setState({data:data,isLoading:false})
             })
           .catch(error => console.log("error", error));
        }
    }
    headerBar() {
        this.setState({header: !this.state.header});
    }

    navigateToScreen()  {
       this.props.navigation.dispatch(DrawerActions.openDrawer());
    }
    goBack() {
        const { navigation } = this.props;
        navigation.goBack();
        navigation.state.params.onSelect({ currentIndex: cardPosition });
    }

    refreshComp()
    {
        this.setState({refreshLoader: true});
        fetch('http://www.chudachuda.com/GetBookmarks?deviceId='+device_id)
        .then(res => res.json())
        .then((responseJson) => {
           this.setState({data:responseJson.feed,refreshLoader:true,refresh:true,currentIndex:0, header: false})
           setTimeout(() => {
              this.setState({refreshLoader:false});
             }, 1000)
            })
    }
    bookmarkItem(id,mobile_id)
    {
        fetch('http://www.chudachuda.com/UpdateBookmark?id='+id+"&mobile_id="+mobile_id+"&category=tamil.thehindu.com")
        .then((response) => {
            var bookmark=JSON.stringify(response);
            var no_of_bookmark=JSON.parse(bookmark);
            var values = no_of_bookmark["_bodyText"];
            currentBookmarkId.push(id);
            no_of_user_bookmark[id]=values;
            this.setState({bookmarkId:currentBookmarkId})
        })
    }
    deviceBookmark() {
        fetch('http://www.chudachuda.com/IsBookmarked?deviceId='+device_id+"&domain=tamil.thehindu.com")
        //fetch('http://chudachudaloadbalancer-822979903.us-east-1.elb.amazonaws.com/IsLiked?deviceId='+device_id+"&domain=tamil.thehindu.com")
        .then((response) => {
            var res=JSON.stringify(response);
            var deviceBookmarkJson=JSON.parse(res);
            var bookmarkedValue = deviceBookmarkJson["_bodyInit"];
            var myArray = eval(bookmarkedValue).join(",").split(',').map(Number);
           this.setState({deviceBookmark:myArray});

        })
      }
    likeItem(id,mobile_id)
    {
        fetch('http://www.chudachuda.com/UpdateLikes?id='+id+"&mobile_id="+mobile_id+"&category=tamil.thehindu.com")
        .then((response) => {
            var like=JSON.stringify(response);
            var no_of_likes=JSON.parse(like);
            var values = no_of_likes["_bodyText"];
            currentLikeId.push(id);
            no_of_user_like[id]=values;
            this.setState({id:currentLikeId})
        })
    }

       deviceLike() {

        fetch('http://www.chudachuda.com/IsLiked?deviceId='+device_id+"&domain=tamil.thehindu.com")
        //fetch('http://chudachudaloadbalancer-822979903.us-east-1.elb.amazonaws.com/IsLiked?deviceId='+device_id+"&domain=tamil.thehindu.com")
        .then((response) => {
            var res=JSON.stringify(response);
            var deviceLikesJson=JSON.parse(res);
            var likedValue = deviceLikesJson["_bodyInit"];
            var myArray = eval(likedValue).join(",").split(',').map(Number);
           this.setState({deviceLike:myArray});

        })
      }
     backPressed = () => {
        Alert.alert(
          'Exit App',
          'Do you want to exit ChudaChuda?',
          [
            {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'Yes', onPress: () => BackHandler.exitApp()},
          ],
          { cancelable: false });
          return true;

      }
      componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
        //clearTimeout(this.cardTimeout);
      }
      componentDidMount(){


          /* Web View */
        var cardPosition = this.props.navigation.getParam('CardPosition');
        if(typeof cardPosition==="undefined")
        {

        }
        else{

            this.setState({currentIndex:cardPosition,isLoading:true});

        }
      }

      componentWillMount() {
       // this.setState({webLoad: false});
          this.setState({header: false});

        this._isMouted = true
        BackHandler.addEventListener('hardwareBackPress', this.backPressed);

         /* Current State Maintain */
        var cardPosition2 = this.props.navigation.getParam('newValue');
        if(typeof cardPosition2==="undefined")
        {
           // console.log("undefined.......");
        }
        else{
            var integer = parseInt(cardPosition2);
            this.setState({currentIndex:integer,isLoading:true, stateLoad: true, header: false})
        }


        this.getJsonData();
        this.deviceLike();
        this.deviceBookmark();
        this.PanResponder = PanResponder.create({

            onStartShouldSetPanResponder: (e, gestureState) => true,
            onPanResponderMove: (evt, gestureState) => {
               if (gestureState.dy > 0 && (this.state.currentIndex > 0)) {//drag down
                    this.swipedCardPosition.setValue({
                        x: 0, y: -SCREEN_HEIGHT + gestureState.dy
                    })
                    //this.setState({header: false})
                    //this.clearTimer();

                }
                else if( (this.state.currentIndex <= this.state.data.length-2)) {// drag up
                   this.position.setValue({ y: gestureState.dy, x: 0 })
                   //this.setState({header: false})
                  // this.clearTimer();

                }
            },
            onPanResponderRelease: (evt, gestureState) => {
                if (this.state.currentIndex > 0 && gestureState.dy > 0 && gestureState.vy > 0) {
                    Animated.timing(this.swipedCardPosition, {
                        toValue: ({ x: 0, y: 0 }),
                        duration: 0
                    }).start(() => {
                    this.setState({ currentIndex: this.state.currentIndex - 1, header: false })
                    this.swipedCardPosition.setValue({ x: 0, y: -SCREEN_HEIGHT })
                    })
                }
                 else if (-gestureState.dy > 0 && -gestureState.vy > 0) {
                 if(this.state.currentIndex <= this.state.data.length-2)
                  {
                    Animated.timing(this.position, {
                        toValue: ({ x: 0, y: -SCREEN_HEIGHT }),
                        duration: 100
                    }).start(() => {
                        this.setState({ currentIndex: this.state.currentIndex + 1, header: false })
                        this.position.setValue({ x: 0, y: 0 })
                    })
                  }
                }
                else if (-gestureState.dy > 0 && -gestureState.vy > 0 &&(this.state.data.length==this.state.currentIndex)) {
                    Animated.timing(this.position, {
                        toValue: ({ x: 0, y: -SCREEN_HEIGHT }),
                        duration: 0
                    }).start(() => {
                        this.setState({ currentIndex: this.state.currentIndex, header: false})
                        this.position.setValue({ x: 0, y: 0 })
                    })
                }
                else {
                    Animated.parallel([
                        Animated.spring(this.position, {
                            toValue: ({ x: 0, y: 0 })
                        }),
                        Animated.spring(this.swipedCardPosition, {
                            toValue: ({ x: 0, y: -SCREEN_HEIGHT })
                        })
                    ]).start()
                }
            }
        })
    }
    renderArticles = () => {


     if(this.state.data.length>0)
        {
        return this.state.data.map((item, i) => {
           const{navigate}=this.props.navigation;
           var len=this.state.data.length;
           var firebase_title=this.props.navigation.getParam("title");







             if (i == this.state.currentIndex -   1) {

                return (
                    <Animated.View key={item.title} style={this.swipedCardPosition.getLayout()}
                        {...this.PanResponder.panHandlers}
                    >
                        <View style={{ flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white' }}>
                            <View style={{ flex: 3, backgroundColor: '#fff', borderColor: '#666', borderWidth: 0.5, width: SCREEN_WIDTH}}>
                                <ImageBackground source={require('./assets/bookmark-bg.jpg')}
                                    style={styles.img}
                                >
                                    <View>
                                        <ImageBackground style={styles.img_header} source={{uri: item.image_url}} onClick = {this.toggleStatus} >
                                            <View style={styles.navbar}>
                                                <View onTouchStart={() => { if(typeof firebase_title ==="undefined"){ this.navigateToScreen();}  else{
                                                        this.navigateToScreen()};}}>
                                                    <Image source={require('./assets/menu.png')} style={{height: 30, width: 30, top: 10, left: 20}} />
                                                </View>
                                                <View onTouchStart={(e) => {this.refreshComp()}}>
                                                    <Image source={require('./assets/refresh.png')} style={{height: 27, width: 27, top: 12, marginLeft: 280}} refreshLoader = {this.state.refreshLoader} />
                                                </View>
                                            </View>
                                        </ImageBackground>
                                    </View>
                                </ImageBackground>
                            </View>
                            <View style={{ flex: 4,paddingTop: 6, paddingBottom: 12, paddingLeft: 12, paddingBottom: 12, paddingRight:10}} onTouchStart = {() => this.headerBar()}>
                                <View >
                                    <Text style={{fontSize: 19, fontWeight: 'bold', marginTop: 0, color: 'black', lineHeight: 30}}>
                                        {item.title}
                                    </Text>
                                    <Text style={{fontSize: 15, lineHeight: 30, marginTop: 10, color: 'black'}}>{item.desc}</Text>
                                </View>
                            </View>
                            <View style={styles.footer}>
                               <View style={{alignSelf: "flex-start", marginTop: 10, marginLeft: 30}}>
                                        <Icon name="ios-share-alt" color="#000" size={25} style={{marginLeft: 17}} onPress={()=>{
                            this.shareMessage(item.title,item.url)}} />
                                </View>
                                <Text style={styles.domain_name}  onPress={() => this.props.navigation.navigate('WebView',{url: item.url})}>மேலும் @ {item.domain}</Text>
                            </View>
                        </View>
                    </Animated.View>
                )
            }

            else if (i < this.state.currentIndex) {
                return null
            }

            if (i == this.state.currentIndex) {
                ComponentName = this.state.ComponentName;
                indexPosition = this.state.currentIndex;
                //AsyncStorage.setItem('cIndex', JSON.stringify(indexPosition));
                //AsyncStorage.setItem('component', JSON.stringify(ComponentName));
                //console.log('isloading content inside render --->'+ this.state.Loading);




                this.state.id.map((likeItems,i) => {
                    if(likeItems == item.id)
                    {
                        isCurrentLiked=true;
                        isCurrentLikedId=item.id;
                       isCurrentLikedValues=no_of_user_like[item.id];

                    }
                });

                this.state.deviceLike.map((likes,i) => {
                        if(likes === item.id)
                        {
                            //console.log("inside map --->",likes);
                                deviceLinkId=likes;
                        }
                });
                this.state.bookmarkId.map((bookmarkItems,i) => {
                    if(bookmarkItems == item.id)
                    {
                        isCurrentBookmarked=true;
                        isCurrentBookmarkId=item.id;
                       isCurrentBookmarkedValues=no_of_user_bookmark[item.id];

                    }
                });

                this.state.deviceBookmark.map((bookmarks,i) => {
                        if(bookmarks === item.id)
                        {
                            //console.log("inside map --->",likes);
                            deviceBookmarkId=bookmarks;
                        }
                });


                return (


                    <Animated.View key={item.title} style={this.position.getLayout()}
                        {...this.PanResponder.panHandlers}
                    >
                            <View style={{ flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white' }}>
                                <View style={{ flex: 3, backgroundColor: '#fff', borderColor: '#666', borderWidth: 0.5, width: SCREEN_WIDTH}} onTouchStart = {() => this.headerBar()}>
                                    <ImageBackground source={require('./assets/bookmark-bg.jpg')}
                                        style={styles.img}
                                    >
                                        <View>
                                            <ImageBackground style={styles.img_header} source={{uri: item.image_url}} onClick = {this.toggleStatus} >
                                                <View style={styles.navbar}>
                                                    <View onTouchStart={() => { if(typeof firebase_title ==="undefined"){ this.navigateToScreen();}  else{
                                                            this.navigateToScreen()};}}>
                                                        <Image source={require('./assets/menu.png')} style={{height: 30, width: 30, top: 10, left: 20}} />
                                                    </View>
                                                    <View onTouchStart={(e) => {this.refreshComp()}}>
                                                        <Image source={require('./assets/refresh.png')} style={{height: 27, width: 27, top: 12, marginLeft: 280}} refreshLoader = {this.state.refreshLoader} />
                                                    </View>
                                                </View>
                                            </ImageBackground>
                                        </View>
                                    </ImageBackground>
                                </View>
                                <View style={{ flex: 4,paddingTop: 6, paddingBottom: 12, paddingLeft: 12, paddingBottom: 12, paddingRight: 10}} onTouchStart = {() => this.headerBar()}>
                                        <View >
                                            <Text style={{fontSize: 19, fontWeight: 'bold', marginTop: 0, color: 'black', lineHeight: 30}}>
                                                {item.title}
                                            </Text>
                                            <Text style={{fontSize: 15, lineHeight: 30, marginTop: 10, color: 'black'}}>{item.desc}</Text>

                                        </View>
                                </View>
                                <View style={styles.footer}>
                                    <View style={{alignSelf: "flex-start", marginTop: 10, marginLeft: 30}}>
                                            <Icon name="ios-share-alt" color="#000" size={25} style={{marginLeft: 17}} onPress={()=>{
                                this.shareMessage(item.title,item.url)}} />
                                    </View>
                                    <Text style={styles.domain_name}  onPress={ ()=>{ Linking.openURL(item.url)}}>{item.domain}</Text>
                                </View>

                        </View>
                    </Animated.View>
                )
            }

            else {

                return (
                    <Animated.View key={item.title} >
                        <View style={{ flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white' }}>
                            <View style={{ flex: 3, backgroundColor: '#fff', borderColor: '#666', borderWidth: 0.5, width: SCREEN_WIDTH}} onPress = {() => this.headerBar()}>
                                <ImageBackground source={require('./assets/bookmark-bg.jpg')}
                                    style={styles.img}
                                >
                                    <View>
                                        <ImageBackground style={styles.img_header} source={{uri: item.image_url}} onClick = {this.toggleStatus} >
                                            <View style={styles.navbar}>
                                                <View onTouchStart={() => { if(typeof firebase_title ==="undefined"){ this.navigateToScreen();}  else{
                                                        this.navigateToScreen()};}}>
                                                    <Image source={require('./assets/menu.png')} style={{height: 30, width: 30, top: 10, left: 20}} />
                                                </View>
                                                <View onTouchStart={(e) => {this.refreshComp()}}>
                                                    <Image source={require('./assets/refresh.png')} style={{height: 27, width: 27, top: 12, marginLeft: 280}} refreshLoader = {this.state.refreshLoader} />
                                                </View>
                                            </View>
                                        </ImageBackground>
                                    </View>
                                </ImageBackground>
                            </View>
                            <View style={{ flex: 4,paddingTop: 6, paddingBottom: 12, paddingLeft: 12, paddingBottom: 12, paddingRight:10}} onTouchStart = {() => this.headerBar()}>
                                <View >
                                    <Text style={{fontSize: 19, fontWeight: 'bold', marginTop: 0, color: 'black', lineHeight: 30}}>
                                        {item.title}
                                    </Text>
                                    <Text style={{fontSize: 15, lineHeight: 30, marginTop: 10, color: 'black'}}>{item.desc}</Text>
                                </View>
                            </View>
                            <View style={styles.footer}>
                                <View style={{alignSelf: "flex-start", marginTop: 10, marginLeft: 30}}>
                                    <Icon name="ios-share-alt" color="#000" size={25} style={{marginLeft: 17}} onPress={()=>{
                            this.shareMessage(item.title,item.url)}} />
                                </View>
                                <Text style={styles.domain_name}  onPress={() => Linking.openURL(item.url)}>மேலும் @ {item.domain}</Text>
                            </View>
                        </View>
                    </Animated.View>
                )
            }


        }).reverse()
    }
    else{
        console.log("is empty...");
        return(
            <View onTouchEnd = {() => this.headerBar()}>
                <ImageBackground  source={require('./assets/sidebar-background.jpg')} style={{height: SCREEN_HEIGHT, width: SCREEN_WIDTH}}>
                    <View style={styles.navbar}>
                        <View onTouchStart={() => { if(typeof firebase_title ==="undefined"){ this.navigateToScreen();}  else{
                                this.navigateToScreen()};}}>
                            <Image source={require('./assets/menu.png')} style={{height: 30, width: 30, top: 10, left: 20}} />
                        </View>                              
                    </View>
                    <View style={styles.background}>
                      <Text style={{color: 'yellow', fontSize: 30, marginTop: 20, textAlign: 'center'}}>You have no Bookmark</Text>
                    </View>
                </ImageBackground>
            </View>
        )


    }

    }

    render() {
        var cardPosition = this.props.navigation.getParam('CardPosition');


      if(this.state.refresh)
      {
            if(this.state.refreshLoader)
           {
              return(
                <View style={{flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white'}}>
                    <View style={{flex: 3, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="650" height="600" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 4, backgroundColor: 'white', width: SCREEN_WIDTH, paddingTop: 6, paddingBottom: 12, paddingLeft: 12}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH - 30}>
                            <Rect x="10" y="20" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="50" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="80" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="120" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="150" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="180" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="210" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="240" width="250" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 1, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="450" height="100" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                </View>
              )
            }
            else{
                return(
                   <View style={{flex: 1}}>
                        {this.renderArticles()}
                   </View>

                )
            }
        }
        else if(this.state.stateLoad) {

            return(
                this.state.isLoading
                ?
                <View style={{flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white'}}>
                    <View style={{flex: 3, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="650" height="600" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 4, backgroundColor: 'white', width: SCREEN_WIDTH, paddingTop: 6, paddingBottom: 12, paddingLeft: 12}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH - 30}>
                            <Rect x="10" y="20" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="50" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="80" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="120" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="150" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="180" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="210" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="240" width="250" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 1, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="450" height="100" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                </View>
                :
                <View style={{ flex: 1 }}>
                    {this.renderArticles()}
                </View>

            )
        }
        else if(this.state.webLoad) {

            return(
                this.state.isLoading
                ?
                <View style={{flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white'}}>
                    <View style={{flex: 3, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="650" height="600" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 4, backgroundColor: 'white', width: SCREEN_WIDTH, paddingTop: 6, paddingBottom: 12, paddingLeft: 12}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH - 30}>
                            <Rect x="10" y="20" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="50" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="80" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="120" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="150" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="180" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="210" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="240" width="250" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 1, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="450" height="100" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                </View>
                :
                <View style={{ flex: 1 }}>
                    {this.renderArticles()}
                </View>

            )



        }
        else {

            return(
                this.state.isLoading
                ?
                <View style={{flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white'}}>
                    <View style={{flex: 3, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="650" height="600" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 4, backgroundColor: 'white', width: SCREEN_WIDTH, paddingTop: 6, paddingBottom: 12, paddingLeft: 12}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH - 30}>
                            <Rect x="10" y="20" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="50" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="80" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="120" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="150" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="180" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="210" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="240" width="250" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 1, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="450" height="100" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                </View>
                :
                <View style={{ flex: 1 }}>
                    {this.renderArticles()}
                </View>

            )
        }
    }
}

export default Bookmark;

const win = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    img:{
        width: null,
        height: null,
        flex: 1
    },
    img_portrait: {
       width: '100%',
       height: '100%'

    },
    header: {
        backgroundColor: '#000',
        opacity: 0.9

    },
    icon: {
        color: '#fff',
        alignItems: 'flex-start',
        margin: 0,
        position: 'relative',
        padding: 12,
        marginTop: 10

    },
    align: {
        alignItems: 'flex-start',
        margin: 0
    },
    fontHeader: {
        alignItems: 'center'
    },
    back: {
        backgroundColor: '#ddd',
        color: '#ff00ff',
        padding: 20
    },
    img_header: {
       height: '100%'
    },
    hour_count: {
        fontSize: 17,
        alignSelf: "flex-end",
        paddingTop:13,
        paddingRight: 10,
        position: 'absolute'
    },
    domain_name: {
        fontSize: 13,
        color: 'blue',
        alignSelf: "flex-end",
        paddingTop:15,
        paddingRight: 10,
        position: 'absolute'
    },
    footer: {
        flex: 1,
        borderTopWidth: 1,
        borderTopColor: '#f1f1f1',
    },
    navbar: {
        height: 50,
        backgroundColor: 'black',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        opacity: 0.6
    },
    background: {
        flex: 1,
        color: 'white',
        paddingTop: 250
    },
    domain: {
        height: 30
    }

});
