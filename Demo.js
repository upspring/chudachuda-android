import React, { Component } from "react";
import { View, Text,StyleSheet,Image,Animated,Dimensions,PanResponder, Linking, ImageBackground, ActivityIndicator,TouchableHighlight} from "react-native";
import { Icon, Button, Container, Header, Content, Left } from 'native-base'
import { DrawerNavigator, StackNavigator, DrawerItems, SafeAreaView,DrawerActions } from 'react-navigation'
const SCREEN_HEIGHT = Dimensions.get("window").height
const SCREEN_WIDTH = Dimensions.get("window").width


class HomeScreen extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: "தினகரன்",
        headerLeft: <Icon name="ios-menu" style={{ paddingLeft: 10 }} onPress={() => navigation.navigate('DrawerOpen')} />,
        drawerLabel: <Text style={{fontSize: 14,color: '#fff', paddingLeft: 15, fontWeight: 'bold', paddingTop: 20, paddingBottom: 20}}>தினகரன்</Text>,
        drawerIcon: ({ tintColor }) => (
          <Image
            source={require('./assets/dinakaran-rounded.png')}
            style={{height: 25, width: 25}}
          />
        ),
      })
       toggleStatus = () =>  {
            console.log('test');
     }
     constructor(props) {
        super(props)

        this.position = new Animated.ValueXY()
        this.swipedCardPosition = new Animated.ValueXY({ x: 0, y: -SCREEN_HEIGHT })
        this.state = {
            currentIndex: 0,
            index:0,
            isLoading: true,
            data:[]

        }


    }

     getJsonData() {
        return fetch('https://www.chudachuda.com/GetCategoryNews?domain=dinakaran.com')
          .then(res => res.json())
          .then((responseJson) => {
              this.setState({data:responseJson.feed,isLoading:false})
              console.log("feed is",responseJson.feed);
          })
          .catch(error => console.log("error", error));
      }
      componentDidMount()
      {

        console.log("component did  mount>>>>>>>>>>>>>>>>>>>>>");

          this.getJsonData();
      }

    componentWillMount() {

        console.log("component will mount>>>>>>>>>>>>>>>>>>>>>");


        this.PanResponder = PanResponder.create({

            onStartShouldSetPanResponder: (e, gestureState) => true,
            onPanResponderMove: (evt, gestureState) => {


                //console.log(gestureState.dy);
                if (gestureState.dy > 0 && (this.state.currentIndex > 0)) {//drag down

                    console.log("down--->");
                    this.swipedCardPosition.setValue({
                        x: 0, y: -SCREEN_HEIGHT + gestureState.dy
                    })
                }
               // else{    this.position.setValue({ y: gestureState.dy }) }


               else if( (this.state.currentIndex <= this.state.data.length-2)) {// drag up
                console.log("up");
                        this.position.setValue({ y: gestureState.dy })
                    }
            },
            onPanResponderRelease: (evt, gestureState) => {
              //  console.log("dataleng",this.state.data.length);

                if (this.state.currentIndex > 0 && gestureState.dy > 50 && gestureState.vy > 0.7) {
                  //  console.log("prev");
                    Animated.timing(this.swipedCardPosition, {
                        toValue: ({ x: 0, y: 0 }),
                        duration: 400
                    }).start(() => {

                        this.setState({ currentIndex: this.state.currentIndex - 1 })
                        this.swipedCardPosition.setValue({ x: 0, y: -SCREEN_HEIGHT })


                    })
                }
                else if (-gestureState.dy > 50 && -gestureState.vy > 0.7) {
                   // console.log("nex");

                    if(this.state.currentIndex <= this.state.data.length-2)//check for last
                    {

                    Animated.timing(this.position, {
                        toValue: ({ x: 0, y: -SCREEN_HEIGHT }),
                        duration: 400
                    }).start(() => {

                        this.setState({ currentIndex: this.state.currentIndex + 1 })
                        this.position.setValue({ x: 0, y: 0 })

                    })
                    console.log("this.state.Index----->",this.state.currentIndex);
                }
                }
                else if (-gestureState.dy > 50 && -gestureState.vy > 0.7 &&(this.state.data.length==this.state.currentIndex)) {
                  //  console.log("equa");
                    Animated.timing(this.position, {
                        toValue: ({ x: 0, y: -SCREEN_HEIGHT }),
                        duration: 400
                    }).start(() => {

                        this.setState({ currentIndex: this.state.currentIndex})
                        this.position.setValue({ x: 0, y: 0 })

                    })
                }
                else {
                  //  console.log("same");
                    Animated.parallel([
                        Animated.spring(this.position, {
                            toValue: ({ x: 0, y: 0 })
                        }),
                        Animated.spring(this.swipedCardPosition, {
                            toValue: ({ x: 0, y: -SCREEN_HEIGHT })
                        })

                    ]).start()

                }
            }
        })
    }
    renderArticles = () => {
      // return ARTICLES.map((item, i) => {
        //  console.log("Image URL" + " "+ARTICLES[i].image_url)
        return this.state.data.map((item, i) => {
            console.log("Image URL"+" "+item.image_url);
            if (i == this.state.currentIndex - 1) {
  
                return (
                    <Animated.View key={item.title} style={this.swipedCardPosition.getLayout()}
                        {...this.PanResponder.panHandlers}
                    >
                    <View style={{ flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white' }}>
  
                        <View style={{ flex: 3, backgroundColor: '#fff', borderColor: '#666', borderWidth: 0.5}}>
                            <ImageBackground source={require('./assets/dinakaran.png')}
                                style={styles.img}
                            >
                                <View>
                                    <ImageBackground style={{height: 270}} source={{uri: item.image_url}} onClick = {this.toggleStatus} >
                                        <View>
                                            <Icon name="ios-menu"  color="#fff" size={25} style={{ position: 'absolute', top: 40, left: 20, color: '#ff9800'}} onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())} />
                                        </View>
                                    </ImageBackground>
                                </View>
                                <View>
                                    <Icon name="ios-menu"  color="#fff" size={25} style={{ position: 'absolute', top: -230, left: 20, color: '#ff9800'}} onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())} />
                                </View>
                            </ImageBackground>
                        </View>
                        <View style={{ flex: 4,paddingTop: 6, paddingBottom: 12, paddingLeft: 12, paddingBottom: 12}}>
                                <View > 
                                    <Text style={{fontSize: 22, fontWeight: 'bold', marginTop: 0, color: 'black'}} onPress={() => Linking.openURL(item.url)}>
                                        {item.title}
                                    </Text>
                                    <Text style={{fontSize: 16, lineHeight: 30, marginTop: 10, color: 'black', height: 180}}>{item.desc}</Text>
                                </View>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{fontSize: 14, paddingLeft: 300, paddingTop:15}}>{item.lastHour}<Text style={{fontSize: 14}}>  Hours Ago</Text></Text>
                            <Text style={{fontSize: 13, color: 'blue', paddingLeft: 15}}  onPress={() => Linking.openURL('http://www.dinakaran.com/')}>{item.domain}</Text>
                        </View>
                    </View>
                    </Animated.View>
                )
            }
            else if (i < this.state.currentIndex) {
                return null
            }
            if (i == this.state.currentIndex) {
  
                return (
  
                    <Animated.View key={item.title} style={this.position.getLayout()}
                        {...this.PanResponder.panHandlers}
                    >
                        <View style={{ flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white' }}>
  
                            <View style={{ flex: 3, backgroundColor: '#fff', borderColor: '#666', borderWidth: 0.5}}>
                                <ImageBackground source={require('./assets/dinakaran.png')}
                                  style={styles.img}
                                >
                                    <View>
                                        <ImageBackground style={{height: 270}} source={{uri: item.image_url}} onClick = {this.toggleStatus} >
                                            <View>
                                                 <Icon name="ios-menu"  color="#fff" size={25} style={{ position: 'absolute', top: 40, left: 20, color: '#ff9800'}} onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())} />
                                            </View>
                                        </ImageBackground>
                                    </View>
                                    <View>
                                        <Icon name="ios-menu"  color="#fff" size={25} style={{ position: 'absolute', top: -230, left: 20, color: '#ff9800'}} onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())} />
                                    </View>
                                </ImageBackground>
                            </View>
                            <View style={{ flex: 4,paddingTop: 6, paddingBottom: 12, paddingLeft: 12, paddingBottom: 12}}>
                                  <View > 
                                      <Text style={{fontSize: 22, fontWeight: 'bold', marginTop: 0, color: 'black'}} onPress={() => Linking.openURL(item.url)}>
                                          {item.title}
                                      </Text>
                                      <Text style={{fontSize: 16, lineHeight: 30, marginTop: 10, color: 'black', height: 180}}>{item.desc}</Text>
                                  </View>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={{fontSize: 14, paddingLeft: 300, paddingTop:15}}>{item.lastHour}<Text style={{fontSize: 14}}>  Hours Ago</Text></Text>
                                <Text style={{fontSize: 13, color: 'blue', paddingLeft: 15}}  onPress={() => Linking.openURL('http://www.dinakaran.com/')}>{item.domain}</Text>
                            </View>
                        </View>
                    </Animated.View>
                )
            }
            else {
  
                return (
                    <Animated.View key={item.title}
  
                    >
                        <View style={{ flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white' }}>
  
                            <View style={{ flex: 3, backgroundColor: '#fff', borderColor: '#666', borderWidth: 0.5}}>
                                <ImageBackground source={require('./assets/dinakaran.png')}
                                    style={styles.img}
                                >
                                    <View>
                                        <ImageBackground style={{height: 270}} source={{uri: item.image_url}} onClick = {this.toggleStatus} >
                                            <View>
                                                <Icon name="ios-menu"  color="#fff" size={25} style={{ position: 'absolute', top: 40, left: 20, color: '#ff9800'}} onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())} />
                                            </View>
                                        </ImageBackground>
                                    </View>
                                    <View>
                                        <Icon name="ios-menu"  color="#fff" size={25} style={{ position: 'absolute', top: -230, left: 20, color: '#ff9800'}} onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())} />
                                    </View>
                                </ImageBackground>
                            </View>
                            <View style={{ flex: 4,paddingTop: 6, paddingBottom: 12, paddingLeft: 12, paddingBottom: 12}}>
                                    <View > 
                                        <Text style={{fontSize: 22, fontWeight: 'bold', marginTop: 0, color: 'black'}} onPress={() => Linking.openURL(item.url)}>
                                            {item.title}
                                        </Text>
                                        <Text style={{fontSize: 16, lineHeight: 30, marginTop: 10, color: 'black', height: 180}}>{item.desc}</Text>
                                    </View>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={{fontSize: 14, paddingLeft: 300, paddingTop:15}}>{item.lastHour}<Text style={{fontSize: 14}}>  Hours Ago</Text></Text>
                                <Text style={{fontSize: 13, color: 'blue', paddingLeft: 15}}  onPress={() => Linking.openURL('http://www.dinakaran.com/')}>{item.domain}</Text>
                            </View>
                        </View>
                    </Animated.View>
                )
  
            }
        }).reverse()

    }
    render() {


        return(

                this.state.isLoading
                ?
                <View style={{flex:1, justifyContent:'center', alignItems:'center', backgroundColor: 'darkgrey'}}>
                    <ActivityIndicator size="large" color="#330066" style={{marginBottom: 10}} animating />
                    <Text style="marginTop: 30, fontSize: 17">Loading...</Text>
                </View>
                :
                <View style={{ flex: 1 }}>
                    {this.renderArticles()}
                </View>
            )

}
}

export default HomeScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    img:{
        width: null,
        height: null,
        flex: 1
    },
    header: {
        backgroundColor: '#000',
        opacity: 0.9

    },
    icon: {
        color: '#fff',
        alignItems: 'flex-start',
        margin: 0,
        position: 'relative',
        padding: 12,
        marginTop: 10

    },
    align: {
        alignItems: 'flex-start',
        margin: 0
    },
    fontHeader: {
        alignItems: 'center'
    },
    back: {
        backgroundColor: '#ddd',
        color: '#ff00ff',
        padding: 20
    }

});
