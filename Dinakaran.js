
import React, { Component } from "react";
import { View, Text,StyleSheet,Image,Animated,Dimensions,PanResponder, Linking, ImageBackground, ActivityIndicator,TouchableHighlight, TouchableOpacity, AsyncStorage, Alert, BackHandler} from "react-native";
import { Icon} from 'native-base'
import {DrawerActions } from 'react-navigation'
const SCREEN_HEIGHT = Dimensions.get("window").height
const SCREEN_WIDTH = Dimensions.get("window").width
const Height_width = Dimensions.get("window");
import renderIf from './renderIf'
import ContentLoader from 'react-native-content-loader'
import Svg, {Circle, Rect } from 'react-native-svg'
import Share from 'react-native-share';
var DeviceInfo = require('react-native-device-info').default;
var device_id=DeviceInfo.getUniqueID();
var currentBookmarkId = [];
var isCurrentBookmarked = false;
var isCurrentBookmarkId = -1;
var no_of_user_bookmark = [];
var isCurrentBookmarkedValues = -1;
var BookmarkArray = [];
var deviceBookmarkId = -1;

var  currentLikeId= [];
var isCurrentLiked=false;
var isCurrentLikedId=-1;
var no_of_user_like=[];
var isCurrentLikedValues =-1;
var deviceLinkId=-1;




class Dinakaran extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'Dinakaran',
        headerLeft: <Icon name="ios-menu" style={{ paddingLeft: 10 }} onPress={() => navigation.navigate('DrawerOpen')} />,
        drawerLabel: <Text style={{fontSize: 14,color: '#fff', paddingLeft: 15, fontWeight: 'bold', paddingTop: 20, paddingBottom: 20}}>தினகரன்</Text>,
        headerTitleStyle:{ color: 'green'},
        drawerIcon: ({ tintColor }) => (
          <Image
            source={require('./assets/dinakaran.png')}
            style={{height: 25, width: 25,borderRadius: 50}}
          />
        ),
      })
       toggleStatus = () =>  {

     }
     constructor(props) {
        super(props)
        this.position = new Animated.ValueXY()
        this.swipedCardPosition = new Animated.ValueXY({ x: 0, y: -SCREEN_HEIGHT })
        this.state = {
            currentIndex: 0,
            index:0,
            isLoading: true,
            refresh:false,
            refreshLoader:true,
            data:[],
            deviceLike:[],
            deviceBookmark: [],
            no_of_likes:0,
            id:[],
            bookmarkId:[],
            header: false,
            ComponentName: 'Dinakaran',
            webLoad: false,
        }
     }
     shareMessage(message,url)
     {
         this.setState({header: false})
         var headline=message;
         let shareOptions = {
              title: "சுடசுட",
              message: headline,
              url:url+"\n"+'\n'+'சுடசுட செயலியை பெற'+'\n'+"http://bit.ly/2ttnEKs",
              subject: "சுடசுட" //  for email
         };
         Share.open(shareOptions);
     }
    getJsonData() {
      this.setState({id:[]});
      currentLikeId=[];
      var title= this.props.navigation.getParam('title');

        if(typeof title==='undefined')
        {
            console.log('main part');

            fetch('http://www.chudachuda.com/GetCategoryNews?domain=dinakaran.com')
            .then(res => res.json())
            .then((responseJson) => {
               this.setState({data:responseJson.feed,isLoading:false})
            })
           .catch(error => console.log("error", error));
        }
        else{
            console.log('notification part')
           fetch('http://www.chudachuda.com/GetCategoryNews?domain=dinakaran.com')
           .then(res => res.json())
           .then((responseJson) => {
           var data=[];
           var JSONObj = {};
           for (var i = 0; i < responseJson.feed.length; i++) {
               var object = responseJson.feed[i];
               for (var property in object) {
                 if(property=="title" && object[property]==title)
                  {

                      for (var property in object) {
                       JSONObj[property]=object[property];
                      }
                     data.push(JSONObj);
                  }
              }
          }
          for(var i = 0; i < responseJson.feed.length; i++)
           {
               var object=responseJson.feed[i]
               for (var property in object) {
                  if(property=="title" && object[property]!=title)
                  {
                     var JSONObj1= {};
                     for (var property in object) {
                        JSONObj1[property]=object[property]
                      }
                      data.push(JSONObj1);
                  }
              }
           }
            this.setState({data:data,isLoading:false})
          })
          .catch(error => console.log("error", error));
        }
    }
    headerBar() {
        this.setState({header: !this.state.header});
    }
    navigateToScreen()
    {
     this.props.navigation.dispatch(DrawerActions.openDrawer());
    }
    refreshComp()
    {
     this.setState({refreshLoader: true});
        fetch('http://www.chudachuda.com/GetCategoryNews?domain=dinakaran.com')
        .then(res => res.json())
        .then((responseJson) => {
                this.setState({data:responseJson.feed,refreshLoader:true,refresh:true,currentIndex:0, header: false})
                    setTimeout(() => {
                        this.setState({refreshLoader:false});
                    }, 1000)
        })
    }
    bookmarkItem(id,mobile_id)
    {
        fetch('http://www.chudachuda.com/UpdateBookmark?id='+id+"&mobile_id="+mobile_id+"&category=dinakaran.com")
        .then((response) => {
            var bookmark=JSON.stringify(response);
            var no_of_bookmark=JSON.parse(bookmark);
            var values = no_of_bookmark["_bodyText"];
            currentBookmarkId.push(id);
            no_of_user_bookmark[id]=values;
            this.setState({bookmarkId:currentBookmarkId, header: false})
        })
    }
    deviceBookmark() {

        fetch('http://www.chudachuda.com/IsBookmarked?deviceId='+device_id+"&domain=dinakaran.com")
        //fetch('http://chudachudaloadbalancer-822979903.us-east-1.elb.amazonaws.com/IsLiked?deviceId='+device_id+"&domain=tamil.thehindu.com")
        .then((response) => {
            var res=JSON.stringify(response);
            var deviceBookmarkJson=JSON.parse(res);
            var bookmarkedValue = deviceBookmarkJson["_bodyInit"];
            var myArray = eval(bookmarkedValue).join(",").split(',').map(Number);
            this.setState({deviceBookmark:myArray});

        })
    }
    likeItem(id,mobile_id)
    {
       fetch('http://www.chudachuda.com/UpdateLikes?id='+id+"&mobile_id="+mobile_id+"&category=dinakaran.com")
       .then(response => response.json())
       .then((response) => {
          var like=JSON.stringify(response);
          var no_of_likes=JSON.parse(like);
          currentLikeId.push(id);
          no_of_user_like[id]=no_of_likes;
          this.setState({id:currentLikeId})
       })
    }

    deviceLike() {
       // fetch('http://chudachudaloadbalancer-822979903.us-east-1.elb.amazonaws.com/IsLiked?deviceId='+device_id)
        fetch('http://www.chudachuda.com/IsLiked?deviceId='+device_id+"&domain=dinakaran.com")
        .then(response => response.json())
        .then((response) => {
           this.setState({deviceLike:response});
        })

      }
      backPressed = () => {
        Alert.alert(
          'Exit App',
          'Do you want to exit ChudaChuda?',
          [
            {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'Yes', onPress: () => BackHandler.exitApp()},
          ],
          { cancelable: false });
          return true;

      }

    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }
    componentDidMount(){
        /* Web View */
       var cardPosition = this.props.navigation.getParam('CardPosition');

        if(typeof cardPosition==="undefined")
        {

        }
        else{
           this.setState({currentIndex:cardPosition,isLoading:true})
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.backPressed);

        var cardPosition2 = this.props.navigation.getParam('newValue');
        /* Current State */
        if(typeof cardPosition2==="undefined")
        {

        }
        else{
           var integer = parseInt(cardPosition2);
           this.setState({currentIndex:integer,isLoading:true})
        }
        this.getJsonData();
        this.deviceLike();
        this.deviceBookmark();
        this.PanResponder = PanResponder.create({
            onStartShouldSetPanResponder: (e, gestureState) => true,
            onPanResponderMove: (evt, gestureState) => {
                if (gestureState.dy > 0 && (this.state.currentIndex > 0)) {//drag down
                   this.swipedCardPosition.setValue({
                        x: 0, y: -SCREEN_HEIGHT + gestureState.dy
                    })
                    //this.setState({header: false});
                }
               else if( (this.state.currentIndex <= this.state.data.length-2)) {// drag up
                   this.position.setValue({ y: gestureState.dy, x: 0 })
                   //this.setState({header: false});

                }
            },
            onPanResponderRelease: (evt, gestureState) => {
                if (this.state.currentIndex > 0 && gestureState.dy > 0 && gestureState.vy > 0) {
                   Animated.timing(this.swipedCardPosition, {
                        toValue: ({ x: 0, y: 0 }),
                        duration: 0
                    }).start(() => {
                    this.setState({ currentIndex: this.state.currentIndex - 1, header: false })
                    this.swipedCardPosition.setValue({ x: 0, y: -SCREEN_HEIGHT })
                    })
                }
                 else if (-gestureState.dy > 0 && -gestureState.vy > 0) {
                  if(this.state.currentIndex <= this.state.data.length-2)
                  {
                    Animated.timing(this.position, {
                        toValue: ({ x: 0, y: -SCREEN_HEIGHT }),
                        duration: 0
                    }).start(() => {
                        this.setState({ currentIndex: this.state.currentIndex + 1, header: false })
                        this.position.setValue({ x: 0, y: 0 })
                    })
                  }
                }
                else if (-gestureState.dy > 0 && -gestureState.vy > 0 &&(this.state.data.length==this.state.currentIndex)) {
                    Animated.timing(this.position, {
                        toValue: ({ x: 0, y: -SCREEN_HEIGHT }),
                        duration: 0
                    }).start(() => {
                        this.setState({ currentIndex: this.state.currentIndex, header: false})
                        this.position.setValue({ x: 0, y: 0 })
                    })
                }
                else {
                    Animated.parallel([
                        Animated.spring(this.position, {
                            toValue: ({ x: 0, y: 0 })
                        }),
                        Animated.spring(this.swipedCardPosition, {
                            toValue: ({ x: 0, y: -SCREEN_HEIGHT })
                        })
                    ]).start()
                }
            }
        })
    }
    renderArticles = () => {
         return this.state.data.map((item, i) => {
          var len=this.state.data.length;
          var firebase_title=this.props.navigation.getParam("title");
            if (i == this.state.currentIndex - 1) {

                return (
                    <Animated.View key={item.title} style={this.swipedCardPosition.getLayout()}
                        {...this.PanResponder.panHandlers}
                    >
                        <View style={{ flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white' }}>
                            <View style={{ flex: 3, backgroundColor: '#fff', borderColor: '#666', borderWidth: 0.5, width: SCREEN_WIDTH}}>
                                <ImageBackground source={require('./assets/dinakaran.png')}
                                    style={styles.img}
                                >
                                    <View>
                                        <ImageBackground style={styles.img_header} source={{uri: item.image_url}} onClick = {this.toggleStatus} >
                                            <View style={styles.navbar}>
                                                <View onTouchStart={() => { if(typeof firebase_title ==="undefined"){ this.navigateToScreen();}  else{
                                                        this.navigateToScreen()};}}>
                                                    <Image source={require('./assets/menu.png')} style={{height: 30, width: 30, top: 10, left: 20}} />
                                                </View>
                                                <View onTouchStart={(e) => {this.refreshComp()}}>
                                                    <Image source={require('./assets/refresh.png')} style={{height: 27, width: 27, top: 12, marginLeft: 280}} refreshLoader = {this.state.refreshLoader} />
                                                </View>
                                            </View>
                                        </ImageBackground>
                                    </View>                                        
                                </ImageBackground>
                            </View>
                            <View style={{ flex: 4,paddingTop: 6, paddingBottom: 12, paddingLeft: 12, paddingBottom: 12, paddingRight: 10}}>
                                <View >
                                    <Text style={{fontSize: 19, fontWeight: 'bold', marginTop: 0, color: 'black', lineHeight: 30}}>
                                        {item.title}
                                    </Text>
                                    <Text style={{fontSize: 15, lineHeight: 30, marginTop: 10, color: 'black'}}>{item.desc}</Text>
                                </View>
                            </View>
                            <View style={styles.footer}>
                                <View style={{lignSelf: "flex-start", marginTop: -5, marginLeft: 20, width: 50}}>
                                      <Icon name="ios-thumbs-up" style={{color: (isCurrentLiked && isCurrentLikedId == item.id || deviceLinkId==item.id )?  "blue" : "black",marginTop: 10, marginLeft: 8}}  size={25} onPress={()=>{ if(deviceLinkId==item.id|| isCurrentLikedId==item.id){console.log("already liked")} else{console.log("new user to like");this.likeItem(item.id,device_id)} }}/>
                                        <Text style={{marginTop:-5, fontSize: 19, marginLeft: 8}}> {(isCurrentLiked &&  isCurrentLikedId == item.id )? isCurrentLikedValues:item.likes}</Text>
                                </View>
                                <View style={{marginTop: -45, marginLeft: 35, width: 50}}>
                                    <Icon name="ios-star" size={35} style={{color: (isCurrentBookmarked && isCurrentBookmarkId == item.id || deviceBookmarkId==item.id )?  "blue" : "black",top: 0, left: 80}}  onPress={()=>{ if(deviceBookmarkId==item.id|| isCurrentBookmarkId==item.id){console.log("already liked")} else{console.log("new user to like");this.bookmarkItem(item.id,device_id)} }} />
                                </View>
                                <View style={{alignSelf: "center"}}>
                                        <Icon name="ios-share-alt" color="#000" size={25} style={{marginTop: -30,marginLeft: 17}} onPress={()=>{
                            this.shareMessage(item.title,item.url)}} />
                                </View>
                                <Text style={styles.domain_name}  onPress={() => Linking.openURL(item.url)}>மேலும் @ {item.domain}</Text>
                            </View>
                        </View>
                    </Animated.View>
                )
            }

            else if (i < this.state.currentIndex) {
                return null
            }


            if (i == this.state.currentIndex) {
                ComponentName = this.state.ComponentName;
                indexPosition = this.state.currentIndex;
                AsyncStorage.setItem('cIndex', JSON.stringify(indexPosition));
                AsyncStorage.setItem('component', JSON.stringify(ComponentName));

                this.state.id.map((likeItems,i) => {

                    if(likeItems == item.id)
                    {
                        isCurrentLiked=true;
                        isCurrentLikedId=item.id;
                       isCurrentLikedValues=no_of_user_like[item.id];

                    }
                });

                this.state.deviceLike.map((likes,i) => {
                    if(likes === item.id)
                    {
                            deviceLinkId=likes;
                    }
                });
                this.state.bookmarkId.map((bookmarkItems,i) => {
                    if(bookmarkItems == item.id)
                    {
                        isCurrentBookmarked=true;
                        isCurrentBookmarkId=item.id;
                       isCurrentBookmarkedValues=no_of_user_bookmark[item.id];

                    }
                });

                this.state.deviceBookmark.map((bookmarks,i) => {
                        if(bookmarks === item.id)
                        {
                            deviceBookmarkId=bookmarks;
                        }
                });

                return (

                    <Animated.View key={item.title} style={this.position.getLayout()}
                        {...this.PanResponder.panHandlers}
                    >
                        <View style={{ flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white' }}>
                            <View style={{ flex: 3, backgroundColor: '#fff', borderColor: '#666', borderWidth: 0.5, width: SCREEN_WIDTH}} onTouchStart = {() => this.headerBar()}>
                                <ImageBackground source={require('./assets/dinakaran.png')}
                                    style={styles.img}
                                >
                                    <View>
                                        <ImageBackground style={styles.img_header} source={{uri: item.image_url}} onClick = {this.toggleStatus} >
                                            <View style={styles.navbar}>
                                                <View onTouchStart={() => { if(typeof firebase_title ==="undefined"){ this.navigateToScreen();}  else{
                                                        this.navigateToScreen()};}}>
                                                    <Image source={require('./assets/menu.png')} style={{height: 30, width: 30, top: 10, left: 20}} />
                                                </View>
                                                <View onTouchStart={(e) => {this.refreshComp()}}>
                                                    <Image source={require('./assets/refresh.png')} style={{height: 27, width: 27, top: 12, marginLeft: 280}} refreshLoader = {this.state.refreshLoader} />
                                                </View>
                                            </View>

                                        </ImageBackground>
                                    </View>
                                </ImageBackground>
                            </View>
                            <View style={{ flex: 4,paddingTop: 6, paddingBottom: 12, paddingLeft: 12, paddingBottom: 12, paddingRight:10}} onTouchStart = {() => this.headerBar()}>
                                <View >
                                    <Text style={{fontSize: 19, fontWeight: 'bold', marginTop: 0, color: 'black', lineHeight: 30}}>
                                        {item.title}
                                    </Text>
                                    <Text style={{fontSize: 15, lineHeight: 30, marginTop: 10, color: 'black'}}>{item.desc}</Text>
                                </View>
                            </View>
                            <View style={styles.footer}>
                                <View style={{lignSelf: "flex-start", marginTop: -5, marginLeft: 20, width: 50}}>
                                      <Icon name="ios-thumbs-up" style={{color: (isCurrentLiked && isCurrentLikedId == item.id || deviceLinkId==item.id )?  "blue" : "black",marginTop: 10, marginLeft: 8}}  size={25} onPress={()=>{ if(deviceLinkId==item.id|| isCurrentLikedId==item.id){console.log("already Bookmarked")} else{console.log("new user to bookmark");this.likeItem(item.id,device_id)} }}/>
                                        <Text style={{marginTop:-5, fontSize: 19, marginLeft: 8}}> {(isCurrentLiked &&  isCurrentLikedId == item.id )? isCurrentLikedValues:item.likes}</Text>
                                </View>
                                <View style={{marginTop: -45, marginLeft: 35, width: 50}}>
                                        <Icon name="ios-star" size={35} style={{color: (isCurrentBookmarked && isCurrentBookmarkId == item.id || deviceBookmarkId==item.id )?  "blue" : "black",top: 0, left: 80}}  onPress={()=>{ if(deviceBookmarkId==item.id|| isCurrentBookmarkId==item.id){console.log("already Bookmarked")} else{console.log("new user to Bookmark");this.bookmarkItem(item.id,device_id)} }} />
                                </View>
                                <View style={{alignSelf: "center"}}>
                                        <Icon name="ios-share-alt" color="#000" size={25} style={{marginTop: -30,marginLeft: 17}} onPress={()=>{
                            this.shareMessage(item.title,item.url)}} />
                                </View>
                                <Text style={styles.domain_name}  onPress={() => Linking.openURL(item.url)}>மேலும் @ {item.domain}</Text>
                            </View>
                        </View>
                    </Animated.View>
                )
            }

            else {

                return (
                    <Animated.View key={item.title} >
                        <View style={{ flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white' }}>
                             <View style={{ flex: 3, backgroundColor: '#fff', borderColor: '#666', borderWidth: 0.5, width: SCREEN_WIDTH}}>
                                <ImageBackground source={require('./assets/dinakaran.png')}
                                    style={styles.img}
                                >
                                    <View>
                                        <ImageBackground style={styles.img_header} source={{uri: item.image_url}} onClick = {this.toggleStatus} >
                                            <View style={styles.navbar}>
                                                <View onTouchStart={() => { if(typeof firebase_title ==="undefined"){ this.navigateToScreen();}  else{
                                                        this.navigateToScreen()};}}>
                                                    <Image source={require('./assets/menu.png')} style={{height: 30, width: 30, top: 10, left: 20}} />
                                                </View>
                                                <View onTouchStart={(e) => {this.refreshComp()}}>
                                                    <Image source={require('./assets/refresh.png')} style={{height: 27, width: 27, top: 12, marginLeft: 280}} refreshLoader = {this.state.refreshLoader} />
                                                </View>
                                            </View>
                                        </ImageBackground>
                                    </View>
                                </ImageBackground>
                            </View>
                            <View style={{ flex: 4,paddingTop: 6, paddingBottom: 12, paddingLeft: 12, paddingBottom: 12, paddingRight: 10}}>
                                <View >
                                    <Text style={{fontSize: 19, fontWeight: 'bold', marginTop: 0, color: 'black', lineHeight: 30}}>
                                        {item.title}
                                    </Text>
                                    <Text style={{fontSize: 15, lineHeight: 30, marginTop: 10, color: 'black'}}>{item.desc}</Text>
                                </View>
                            </View>
                            <View style={styles.footer}>
                                <View style={{lignSelf: "flex-start", marginTop: -5, marginLeft: 20, width: 50}}>
                                      <Icon name="ios-thumbs-up" style={{color: (isCurrentLiked && isCurrentLikedId == item.id || deviceLinkId==item.id )?  "blue" : "black",marginTop: 10, marginLeft: 8}}  size={25} onPress={()=>{ if(deviceLinkId==item.id|| isCurrentLikedId==item.id){console.log("already liked")} else{console.log("new user to like");this.likeItem(item.id,device_id)} }}/>
                                        <Text style={{marginTop:-5, fontSize: 19, marginLeft: 8}}> {(isCurrentLiked &&  isCurrentLikedId == item.id )? isCurrentLikedValues:item.likes}</Text>
                                </View>
                                <View style={{marginTop: -45,marginLeft: 35, width: 50}}>
                                    <Icon name="ios-star" size={35} style={{color: (isCurrentBookmarked && isCurrentBookmarkId == item.id || deviceBookmarkId==item.id )?  "blue" : "black",top: 0, left: 80}}  onPress={()=>{ if(deviceBookmarkId==item.id|| isCurrentBookmarkId==item.id){console.log("already liked")} else{console.log("new user to like");this.bookmarkItem(item.id,device_id)} }} />
                                </View>
                                <View style={{alignSelf: "center"}}>
                                        <Icon name="ios-share-alt" color="#000" size={25} style={{marginTop: -30,marginLeft: 17}} onPress={()=>{
                            this.shareMessage(item.title,item.url)}} />
                                </View>
                                <Text style={styles.domain_name}  onPress={() => Linking.openURL(item.url)}>மேலும் @ {item.domain}</Text>
                            </View>
                        </View>
                    </Animated.View>
                )
            }

        }).reverse()

    }
    render() {
        if(this.state.refresh)
        {
          if(this.state.refreshLoader)
           {
              return(
                <View style={{flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white'}}>
                    <View style={{flex: 3, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="650" height="600" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 4, backgroundColor: 'white', width: SCREEN_WIDTH, paddingTop: 6, paddingLeft: 12}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH - 30}>
                            <Rect x="10" y="20" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="50" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="80" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="120" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="150" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="180" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="210" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="240" width="250" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 1, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="450" height="100" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                </View>
              )
            }
            else{

                return(
                   <View style={{flex: 1}}>
                        {this.renderArticles()}
                   </View>
                )
            }
        }
        else if(this.state.webLoad) {
            return(
                this.state.isLoading
                ?
                <View style={{flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white'}}>
                    <View style={{flex: 3, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="650" height="600" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 4, backgroundColor: 'white', width: SCREEN_WIDTH, paddingTop: 6, paddingLeft: 12}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH - 30}>
                            <Rect x="10" y="20" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="50" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="80" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="120" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="150" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="180" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="210" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="240" width="250" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 1, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="450" height="100" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                </View>
                :
                <View style={{ flex: 1 }}>
                    {this.renderArticles()}
                </View>
            )
        }
        else
        {
           return(

                this.state.isLoading
                ?
                <View style={{flex: 1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white'}}>
                    <View style={{flex: 3, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="650" height="600" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 4, backgroundColor: 'white', width: SCREEN_WIDTH, paddingTop: 6, paddingLeft: 12}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH - 30}>
                            <Rect x="10" y="20" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="50" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="80" width="450" height="10" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="120" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="150" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="180" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="210" width="450" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                            <Rect x="10" y="240" width="250" height="7" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                    <View style={{flex: 1, backgroundColor: '#fff', width: SCREEN_WIDTH}}>
                        <ContentLoader  primaryColor="#f7f7f7" secondaryColor="#bdbdbd" height={SCREEN_HEIGHT} width={SCREEN_WIDTH}>
                            <Rect x="0" y="0" width="450" height="100" stroke="#ddd" rotate="180" strokeWidth="2" fill="#ddd" />
                        </ContentLoader>
                    </View>
                </View>
                :
                <View style={{ flex: 1 }}>
                    {this.renderArticles()}
                </View>
            )
      }

    }
}

export default Dinakaran;

const win = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    img:{
        width: null,
        height: null,
        flex: 1
    },
    header: {
        backgroundColor: '#000',
        opacity: 0.9

    },
    icon: {
        color: '#fff',
        alignItems: 'flex-start',
        margin: 0,
        position: 'relative',
        padding: 12,
        marginTop: 10

    },
    align: {
        alignItems: 'flex-start',
        margin: 0
    },
    fontHeader: {
        alignItems: 'center'
    },
    back: {
        backgroundColor: '#ddd',
        color: '#ff00ff',
        padding: 20
    },
    img_header: {
       height: '100%'
    },
    hour_count: {
        fontSize: 17,
        alignSelf: "flex-end",
        paddingTop:13,
        paddingRight: 10,
        position: 'absolute'
    },
    domain_name: {
        fontSize: 13,
        color: 'blue',
        alignSelf: "flex-end",
        paddingTop:15,
        paddingRight: 10,
        position: 'absolute'
    },
    footer: {
        flex: 1,
        borderTopWidth: 1,
        borderTopColor: '#f1f1f1',

    },
    navbar: {
        height: 50,
        backgroundColor: 'black',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        opacity: 0.6


    },
    navbar1: {
        marginTop: -280,
        height: 50,
        backgroundColor: 'black',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        opacity: 0.8


    },
    domain: {
        height: 30
    }

});
