import React, { Component } from "react";
import {Header, Body, Container, Icon, Content, } from 'native-base'
import { DrawerNavigator, StackNavigator, DrawerItems, SafeAreaView } from 'react-navigation'
import { createDrawerNavigator } from 'react-navigation'
import  Samayam  from './Samayam'
import TamilTheHindu from './TamilTheHindu'
import WebView from './webView'
import Share from './share';
import Dinakaran from './Dinakaran'
import BBCTamil from './BBCTamil'
import Bookmark from './Bookmark'
import About from './About'
import Notification from './Notification'

import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    Image,
    ImageBackground,
    Animated,
    PanResponder,
    TouchableHighlight
} from "react-native";

const customDrawerContentComponent = (props) =>(
    <Container style={{backgroundColor: '#01648e'}}>
         <ImageBackground source = {require('./assets/about-background.jpg')} style={{heigth: 50, zIndex: 1   }}>
             <TouchableHighlight>
                 <Image
                         style = {{marginTop: 30, marginLeft: 70, marginBottom: 30}}
                         source ={require('./assets/logo-white.png')}
                 >
                 </Image>
             </TouchableHighlight>
         </ImageBackground>

         <Content style={{zIndex: 1, marginTop: 0}}>
             <ImageBackground source = {require('./assets/about-background.jpg')} style={{height: 700}}>
                 <View style={{marginTop: 20, marginBottom: 20}}>
                     <DrawerItems {...props} />
                 </View>
             </ImageBackground>
         </Content>
     </Container>


 )

const MySide = createDrawerNavigator({

    TamilTheHindu: {
        screen: TamilTheHindu
    },
    Dinakaran:{
        screen: Dinakaran
    },
    BBCTamil: {
        screen: BBCTamil
    },
    Samayam: {
        screen: Samayam
    },
    Bookmark: {
        screen: Bookmark,
        header: null
    },
    About: {
        screen: About,
        header: null
    },
    WebView: {
        screen: WebView
    },
    Notification:
    {
        screen:Notification,
        header:null
    }
    
},{
    initialRouteName: 'TamilTheHindu',
    contentComponent: customDrawerContentComponent,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
    contentOptions: {
    activeBackgroundColor: 'rgba(0, 0, 0, 0.2)',
        inactiveTintColor: 'black',
        inactiveBackgroundColor: 'transparent',
        header: null
    }
})
export default MySide;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'green'
    },
    img:{
        width:450,
        height:450,
        flex: 1
    }
});
