package com.upspring.chudachuda;

import com.facebook.react.ReactActivity;

import android.content.Intent;

import android.os.Bundle;

import com.reactnativecomponent.splashscreen.RCTSplashScreen;

public class MainActivity extends ReactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        RCTSplashScreen.openSplashScreen(this);   //open splashscreen
        super.onCreate(savedInstanceState);
    }


    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "chudachudaReact";
    }

     @Override
   public void onNewIntent (Intent intent) {
     super.onNewIntent(intent);
       setIntent(intent);
   }
}
