package com.upspring.chudachuda;


import android.app.Application;

import com.facebook.react.ReactApplication;

import com.reactnativecomponent.splashscreen.RCTSplashScreenPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.horcrux.svg.SvgPackage;

import com.evollu.react.fcm.FIRMessagingPackage;
import cl.json.RNSharePackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;



import java.util.Arrays;
import java.util.List;
import com.facebook.react.ReactActivity;
import android.content.Intent;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),


            new RCTSplashScreenPackage(),
            new ReactVideoPackage(),

            new RNDeviceInfo(),
            new SvgPackage(),




            new RNSharePackage(),
              //register Module
            //new MapsPackage(),
            new FIRMessagingPackage(),

              new NetInfoPackage(),
              new RNCWebViewPackage()

      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }

}
