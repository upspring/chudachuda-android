import React from 'react'
import List from './List.js'
import {NetworkProvider} from 'react-native-offline'
import Network from './Network'


const App = () => {
   return (
      <NetworkProvider>
          <Network />
      </NetworkProvider>  
    
    )
}
export default App