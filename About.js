import React, { Component } from "react";
import { View, Text,StyleSheet,Image,Animated,Dimensions,PanResponder, Linking, ImageBackground, ActivityIndicator,TouchableHighlight, TouchableOpacity, AsyncStorage, BackHandler, Alert, } from "react-native";
import { Icon, Button, Container, Header, Content, Left } from 'native-base'
import { DrawerNavigator, StackNavigator, DrawerItems, SafeAreaView,DrawerActions } from 'react-navigation'
const SCREEN_HEIGHT = Dimensions.get("window").height
const SCREEN_WIDTH = Dimensions.get("window").width
import renderIf from './renderIf'

class About extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: "தமிழ் தி இந்து",
        headerLeft: <Icon name="ios-menu" style={{ paddingLeft: 10 }} onPress={() => navigation.navigate('DrawerOpen')} />,
        drawerLabel: <Text style={{fontSize: 14,color: '#fff', paddingLeft: 15, fontWeight: 'bold', paddingTop: 20, paddingBottom: 20}}>எங்களை பற்றி அறிய</Text>,
        drawerIcon: ({ tintColor }) => (
          <Image
            source={require('./assets/info.png')}
            style={{height: 25, width: 25, borderRadius: 50}}
          />
        ),
      })
       toggleStatus = () =>  {
    }
    constructor(props) {
      super(props)
      this.position = new Animated.ValueXY()
      this.swipedCardPosition = new Animated.ValueXY({ x: 0, y: -SCREEN_HEIGHT })
      this.state = {
          currentIndex: 0,
          index:0,
          isLoading: true,
          refresh:false,
          refreshLoader:true,
          data:[],
          deviceLike:[],
          no_of_likes:0,
          id:[],
          header: false,
          webLoad: false,
          stateLoad: false,
          stateLoader: false,
          ComponentName:"TamilTheHindu"
      };

    }
    backPressed = () => {
      Alert.alert(
        'Exit App',
        'Do you want to exit ChudaChuda?',
        [
          {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Yes', onPress: () => BackHandler.exitApp()},
        ],
        { cancelable: false });
        return true;

    }
    componentWillUnmount(){
      BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
      //clearTimeout(this.cardTimeout);
    }
    componentWillMount(){
        this._isMouted = true
        BackHandler.addEventListener('hardwareBackPress', this.backPressed);
    }
    headerBar() {
      this.setState({header: !this.state.header});
      console.log("this is header"+this.state.header);
      /* this.timeout = setTimeout(() =>{
          this.setState({header: false});
          console.log("header False"+this.state.header);
      }, 6000);*/

    }
    navigateToScreen()  {
        this.props.navigation.dispatch(DrawerActions.openDrawer());
    }

    render(){
        return(
          <View onTouchEnd = {() => this.headerBar()}>
              <ImageBackground  source={require('./assets/sidebar-background.jpg')} style={{height: SCREEN_HEIGHT, width: SCREEN_WIDTH}}>
                  {renderIf(this.state.header)(
                      <View style={styles.navbar}>
                          <View onTouchStart={() => { if(typeof firebase_title ==="undefined"){ this.navigateToScreen();}  else{
                                  this.navigateToScreen()};}}>
                              <Image source={require('./assets/menu.png')} style={{height: 30, width: 30, top: 10, left: 20}} />
                          </View>
                      </View>

                  )}
                  <View style={styles.background}>
                      <Text style={{fontSize:40, color: '#00C851', textAlign: 'center'}}>Contact Us</Text>
                      <Text style={{color: 'yellow', fontSize: 30, marginTop: 20, textAlign: 'center'}}>Upspring Infotech P Ltd</Text>
                      <View style={{marginTop: 50, marginLeft: 60}}><Icon name="ios-pin" size={25} style={{paddingLeft: 10, color: 'white', position: 'absolute'}}></Icon><Text style={{marginLeft: 50,fontSize:17, paddingLeft:5, color: 'white'}}>No 2 Reddi Rao Tank South,</Text></View>
                      <Text style={{fontSize:17, paddingLeft:115, color: 'white'}}>Kumbakonam - 612001</Text>
                      <View style={{marginTop: 30, marginLeft: 60}}><Icon name="ios-mail" size={25} style={{paddingLeft: 10,color: 'white', position: 'absolute'}}></Icon><Text style={{paddingLeft: 5,marginLeft: 50,fontSize:17, color: 'white'}}>info@upspring.it</Text></View>
                      <View style={{marginTop: 40, marginLeft: 60}}><Icon name="ios-phone-portrait" size={25} style={{paddingLeft: 13,color: 'white', position: 'absolute'}}></Icon><Text style={{paddingLeft:5, marginLeft: 50,fontSize:17, color: 'white'}}>+91 8903411674</Text></View>
                  </View>
              </ImageBackground>
          </View>

        )
    }
}
export default About;

const styles = StyleSheet.create({
  background: {
      flex: 1,
      color: 'white',
      paddingTop: 150
  },
  navbar: {
    height: 50,
    backgroundColor: 'black',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    opacity: 0.6
  },


});
