import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet, 
    Image, TouchableOpacity
} from "react-native"
import { Icon, Button, Container, Header, Content, Left } from 'native-base'
import { DrawerNavigator, StackNavigator, DrawerItems, SafeAreaView,DrawerActions } from 'react-navigation'



class Header1 extends Component {
    static navigationOptions = {
        title: 'Welcome',
      };
    render() {
       /* const { navigate } = this.props.navigation;*/
        return(
            <View style={styles.header}>
                <Left style={styles.align}>
                        <Icon style={{color: '#fff', marginLeft: -100, opacity: 0.9 }} name="ios-menu" onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}>
                        </Icon>
                    </Left>
                <Text style={{fontSize: 20}}> Header Part</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    header: {
        backgroundColor: 'gold',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50
    }
    
});

export default Header1